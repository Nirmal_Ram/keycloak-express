'use strict';

const Keycloak = require('keycloak-connect');
const express = require('express');
const session = require('express-session');

const MongoStore = require('connect-mongo');
const app = express();

var store = MongoStore.create({ mongoUrl: 'mongodb://localhost/test-app' });
var keycloak = new Keycloak({ store: store });

//session
app.use(session({
  secret:'thisShouldBeLongAndSecret',
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
  },
  resave: true,
  saveUninitialized: true,
  store: MongoStore.create({ mongoUrl: 'mongodb://localhost/test-app' })
}));

app.use(keycloak.middleware());

//route protected with Keycloak
app.get('/test', keycloak.protect(), function(req, res){
  res.send('Access Granted . You are authenticated');
});

app.get('/device', keycloak.checkSso(), function(req, res){
  res.send('Access Granted . You are get list of all devices');
});

// protected roles 
// realm - test
// client1 - keycloak-express
// client2 - keycloak-express1
// user1 - keycloak
// user2 - keycloak2

app.get('/realm-admin1', keycloak.protect('realm:realm-admin1'),(req,res)=>{
  res.send('Realm access granted')
})

app.get('/realm-admin2', keycloak.protect('realm:realm-admin2'),(req,res)=>{
  res.send('Realm access granted')
})

app.get('/client1-admin', keycloak.protect('client1-admin'),(req,res)=>{
  res.send('client admin access granted')
})

app.get('/client2-admin', keycloak.protect('keycloak-express2:client2-admin'),(req,res)=>{
  res.send('client admin access granted')
})


app.get('/client1-user', keycloak.protect('client1-user'),(req,res)=>{
  res.send('client user access granted')
})


app.get('/client2-user', keycloak.protect('keycloak-express2:client2-user'),(req,res)=>{
  res.send('client user access granted')
})


//unprotected route
app.get('/',function(req,res){

  res.send('token is now generated. you may continue with protected routes : /test');
});

app.use( keycloak.middleware( { logout: '/' } ));

app.listen(8000, function () {
  console.log('Listening at http://localhost:8000');
});